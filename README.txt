Ensembl Upgrade Project
The application data version is on https://www-ensembl.stemformatics.org/
The base direction: /mnt/data/repo/git-working/5001ensemblup/ensemblupgrade/
===========

Pre-requisites (for Ubuntu)
--------------
- Python 3



Getting Started
---------------
Step 1: Please establish and activate virtual environment by the command line:
python3 -m venv env
source env/bin/activate

Step 2: install some useful tools 
pip install "pyramid==1.9.1"
pip install -e .
pip install pyramid_handlers
pip install pymysql

Step 3: Now you can run the application by the command line:
pserve development.ini

New ways of doing things from Rowland
-------------------------------------
There is a main repository called ensemblupgrade. This repo is the one that Luxin setup for the Ensembl Upgrade Website. Rowland has setup code here to create the mapping files to be used later in the process.

There is code in s4m_pyramid (the main stemformatics website) that allows you how to upgrade the gene lists within Stemformatics and update the private_gene_sets_update_archive. This relies on the mappings files created in the ensemblupgrade repo.

This repo has a Pull Request that provides a unit test. This unit test shows how you can use the repo to create the mappings files. The Pull Request is #24:
https://bitbucket.org/stemformatics/ensemblupgrade/pull-requests/25


Old ways of doing things from Luxin
------------------------------------
How to generate final mapping pkl file in Ensembl Upgrade?
/5001ensemblup/ensemblupgrade/ensemblup/models/final_mapping_model.py
The file final_mapping_mapping.py is used to generate the final mapping pkl between any two versions in Ensembl Database.
The command line is:
python final_mapping_model.py homo_sapiens_core_69_37 homo_sapiens_core_86_38 homo_sapiens_core_86_38 yes yes

By applying the command line, the script will generate the final pkl file saved in directory: /5001ensemblup/ensemblupgrade/ensemblup/database

How to generate full gene list file in Ensembl Upgrade?
/5001ensemblup/ensemblupgrade/ensemblup/models/generate_pkl_file.py
The filegenerate_pkl_file.py is used to generate the full gene list with details for any versions in Ensembl Database.
The command line is:
python generate_pkl_file.py homo_sapiens_core_69_37 homo_sapiens_core_86_38 homo_sapiens_core_86_38 yes yes

By applying the command line, the script will generate the full gene list with details  file saved in directory: /5001ensemblup/ensemblupgrade/ensemblup/database

How to run unit test in Ensemble Upgrade?
/5001ensemblup/ensemblupgrade/ensemblup/models/unit_file.py
The command line: python -m unittest unit_test.py


