import os
# Web path
web_path = "https://dev-dp-pyramid.stemformatics.org"
stemformatics_web_path = 'https://www.stemformatics.org/'

# Site name
site_name =  'Stemformatics'

# version number
ensembl_upgrade_version = 'v0.1'

# Add all base web information here
web_info = {
    'web_path': web_path,
    'stemformatics_web_path': stemformatics_web_path,
    'site_name': site_name,
    'ensembl_upgrade_version': ensembl_upgrade_version
}

# Database connection string
psycopg2_conn_string = "dbname=public_data user=portaladmin"

base_data_dir = os.getcwd() + "/ensemblup/database/"


