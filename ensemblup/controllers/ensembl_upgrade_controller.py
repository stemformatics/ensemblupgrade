from pyramid_handlers import action
from pyramid.response import Response
from pyramid.httpexceptions import HTTPFound
from pyramid.config import Configurator
from ensemblup.config import *
from ensemblup.models.ensembl_upgrade_model import EnsemblUpgradeModel
import csv


class EnsemblUpgradeController:


    def __init__(self,request):
        self.request=request
        self.response=request.response


    @action(renderer='templates/compare_diff_btw_two_versions.mako')
    def home_page(self):
        list_of_mappings = EnsemblUpgradeModel.get_list_of_previous_mappings()
        return {'project': 'EnsemblUpgrade',
                'list_of_mappings':list_of_mappings,
                'web_info': web_info}

    @action(renderer="templates/summary_report_with_final_mapping.mako")
    def generate_summary_report_with_final_mapping(self):
        first_version = self.request.params['first_version']
        second_version = self.request.params['second_version']

        force_refresh = 'no'
        debug = False
        summary = EnsemblUpgradeModel.get_summary_report_with_final_mapping(first_version, second_version, debug, force_refresh)

        return {'summary': summary,
                'first_version': first_version,
                'second_version': second_version,
                'web_info': web_info}

    @action(renderer="templates/show_full_genes_list_details.mako")
    def show_full_genes_list_details(self):
        ensembl_version = self.request.params['version']

        full_gene_list = EnsemblUpgradeModel.show_full_genes_list_details(ensembl_version)

        return{'examples': full_gene_list,
               'web_info': web_info,
               'version': ensembl_version}


    @action(renderer="templates/list_all_removed_genes.mako")
    def get_removed_gene_details(self):

        first_version = self.request.params['first_version']
        second_version = self.request.params['second_version']

        EnsemblUpgradeModel.get_all_removed_gene_details(first_version, second_version)

        gene_name_list = EnsemblUpgradeModel.categorise_removed_genes(first_version, second_version)

        return{'web_info': web_info,
               'examples': gene_name_list[0],
               'uncategorised': gene_name_list[3],
               'haplotype': gene_name_list[4],
               'entrez': gene_name_list[5],
               'uncategorised_percent': gene_name_list[6],
               'total_num': gene_name_list[7],
               'first_version': first_version,
               'second_version': second_version}


    @action(renderer="templates/list_all_added_genes.mako")
    def get_added_gene_details(self):

        first_version = self.request.params['first_version']
        second_version = self.request.params['second_version']

        gene_name_list = EnsemblUpgradeModel.get_all_added_gene_details(first_version, second_version)

        return{'web_info': web_info,
               'examples': gene_name_list,
               'first_version': first_version,
               'second_version': second_version}


    @action(renderer="templates/mapping_history.mako")
    def mapping_history(self):
        first_version = self.request.params['first_version']
        second_version = self.request.params['second_version']

        mapping_history_list = EnsemblUpgradeModel.get_mapping_history(first_version, second_version)

        return {'web_info': web_info,
                'examples': mapping_history_list,
                'first_version': first_version,
                'second_version': second_version}


    @action(renderer="templates/gene_mapping_other_stable_ids.mako")
    def genes_mapped_to_other_genes(self):
        first_version = self.request.params['first_version']
        second_version = self.request.params['second_version']

        gene_mapping_list = EnsemblUpgradeModel.get_genes_mapped_to_other_genes(first_version, second_version)

        return {'web_info': web_info,
                'examples': gene_mapping_list,
                'first_version': first_version,
                'second_version': second_version}

    @action(renderer="templates/gene_mapping_other_stable_ids.mako")
    def full_final_mapping(self):
        first_version = self.request.params['first_version']
        second_version = self.request.params['second_version']

        gene_mapping_list = EnsemblUpgradeModel.get_full_final_mapping(first_version, second_version)

        return {'web_info': web_info,
                'examples': gene_mapping_list,
                'first_version': first_version,
                'second_version': second_version}


    @action(renderer="templates/removed_genes_final_mapping.mako")
    def removed_genes_final_mapping(self):
        first_version = self.request.params['first_version']
        second_version = self.request.params['second_version']

        gene_mapping_list = EnsemblUpgradeModel.removed_genes_final_mapping(first_version, second_version)

        return {'web_info': web_info,
                'examples': gene_mapping_list,
                'first_version': first_version,
                'second_version': second_version}
