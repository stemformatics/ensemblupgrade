import unittest
import os
import _pickle as cPickle
from pathlib import Path
from ensemblup.models.ensembl_upgrade_model import EnsemblUpgradeModel


import sys
import pdb
import functools
import traceback
def debug_on(*exceptions):
    if not exceptions:
        exceptions = (AssertionError, )
    def decorator(f):
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except exceptions:
                info = sys.exc_info()
                traceback.print_exception(*info) 
                pdb.post_mortem(info[2])
        return wrapper
    return decorator



# https://docs.pylonsproject.org/projects/pyramid/en/latest/narr/testing.html

old_gene_ensembl_version = 'homo_sapiens_core_69_37'
new_gene_ensembl_version = 'homo_sapiens_core_91_38'
force_refresh = 'no'

class TestStringMethods(unittest.TestCase):
 
    @debug_on()
    def test_basics_v69_human(self):
        force_refresh = 'no'
        old_gene_ensembl_version = 'homo_sapiens_core_69_37'
        result_old = EnsemblUpgradeModel.get_full_list_of_genes(old_gene_ensembl_version,force_refresh)
        full_stable_ids = result_old[0]
        gene_ids_to_entrez_id_mapping = result_old[1]

        assert len(result_old) == 2
        assert len(full_stable_ids) == 60620
        assert 'ENSG00000115415' in full_stable_ids
        assert 'ENSG00000267380' in full_stable_ids
        assert 'ENSG00000276681' not in full_stable_ids
        assert gene_ids_to_entrez_id_mapping['ENSG00000164404']==[2661]
        assert gene_ids_to_entrez_id_mapping['ENSG00000115415']==[6772]


    @debug_on()
    def test_basics_v91_human(self):
        force_refresh = 'no'
        new_gene_ensembl_version = 'homo_sapiens_core_91_38'
        result_new = EnsemblUpgradeModel.get_full_list_of_genes(new_gene_ensembl_version,force_refresh)
        full_stable_ids = result_new[0]
        gene_ids_to_entrez_id_mapping = result_new[1]

        assert len(result_new) == 2
        assert len(full_stable_ids) == 64661
        assert 'ENSG00000115415' in full_stable_ids
        assert 'ENSG00000267380' not in full_stable_ids
        assert 'ENSG00000276681' in full_stable_ids
        assert gene_ids_to_entrez_id_mapping['ENSG00000164404']==[2661]
        assert gene_ids_to_entrez_id_mapping['ENSG00000115415']==[6772]


    # This test is for v69 to v91 to ensure it's working
    @debug_on()
    def test_v69_to_v91_human(final_mapping):

        old_gene_ensembl_version = 'homo_sapiens_core_69_37'
        new_gene_ensembl_version = 'homo_sapiens_core_91_38'
        force_refresh = 'no'

        result_old = EnsemblUpgradeModel.get_full_list_of_genes(old_gene_ensembl_version,force_refresh)
        old_list_of_genes = result_old[0] 
        old_gene_entrez_mapping_dict = result_old[1] 

        result_new = EnsemblUpgradeModel.get_full_list_of_genes(new_gene_ensembl_version,force_refresh)
        new_list_of_genes = result_new[0] 
        new_gene_entrez_mapping_dict = result_new[1] 

        final_mapping_without_entrez = EnsemblUpgradeModel.iterate_to_find_final_mapping(old_list_of_genes,old_gene_ensembl_version,new_gene_ensembl_version,force_refresh)

        assert final_mapping_without_entrez['ENSG00000115415'] == ['ENSG00000115415']
        assert final_mapping_without_entrez['ENSG00000206713'] == ['ENSG00000206713']
        assert final_mapping_without_entrez['ENSG00000241465'] == ['None']
        assert final_mapping_without_entrez['ENSG00000267380'] == ['None']

        result = EnsemblUpgradeModel.rescue_old_genes_by_entrez(old_gene_ensembl_version,new_gene_ensembl_version,final_mapping_without_entrez,old_gene_entrez_mapping_dict,new_gene_entrez_mapping_dict)
        final_mapping_with_entrez = result[0]
        summary = result[1]
        no_mappings = summary['no_mappings']
        mappings = summary['mappings']
        rescued_with_entrez = summary['rescued_with_entrez']

        assert len(final_mapping_with_entrez) == 60620
        

        assert final_mapping_with_entrez['ENSG00000115415'] == ['ENSG00000115415']
        assert final_mapping_with_entrez['ENSG00000206713'] == ['ENSG00000206713']
        assert len(final_mapping_with_entrez['ENSG00000241465'])==3
        assert 'ENSG00000236362' in final_mapping_with_entrez['ENSG00000241465']
        assert 'ENSG00000215269' in final_mapping_with_entrez['ENSG00000241465']

        assert final_mapping_without_entrez['ENSG00000267380'] == ['None']
 
        # This is all testing from v69 to v91 human
        assert no_mappings==6204 # was 5741 just before I moved over to the model
        assert mappings==54416 # was 54879 just before I moved over to the model
        assert rescued_with_entrez==971 # was 797 just before I moved over to the model

        assert len(final_mapping_with_entrez['ENSG00000184348']) == 8 
        assert u'ENSG00000277075' in final_mapping_with_entrez['ENSG00000184348'] 
        assert u'ENSG00000276903' in final_mapping_with_entrez['ENSG00000184348']



        assert 'ENSG00000007216' in final_mapping_with_entrez['ENSG00000262654']

        assert 'ENSG00000240292' in final_mapping_with_entrez
        assert final_mapping_with_entrez['ENSG00000240292'] == ['No mapping']

        assert 'ENSG00000266819' in final_mapping_with_entrez
        assert final_mapping_with_entrez['ENSG00000266819'] == ['No mapping']

        result = EnsemblUpgradeModel.dump_out_mappings(final_mapping_with_entrez,old_gene_ensembl_version,new_gene_ensembl_version)
        assert result == True




        debug = 'False'
        force_refresh = 'no'
        summary = EnsemblUpgradeModel.get_summary_report_with_final_mapping(old_gene_ensembl_version, new_gene_ensembl_version, debug, force_refresh)

        assert summary['removed_id'] == 6204
        assert summary['added_id'] == 10855
        assert summary['same_id_mapping'] == 52354

        return 

    @debug_on()
    def test_v86_to_v91_human(final_mapping):
        old_gene_ensembl_version = 'homo_sapiens_core_86_38'
        new_gene_ensembl_version = 'homo_sapiens_core_91_38'
        force_refresh = 'no'

        result_old = EnsemblUpgradeModel.get_full_list_of_genes(old_gene_ensembl_version,force_refresh)
        old_list_of_genes = result_old[0] 
        old_gene_entrez_mapping_dict = result_old[1] 

        result_new = EnsemblUpgradeModel.get_full_list_of_genes(new_gene_ensembl_version,force_refresh)
        new_list_of_genes = result_new[0] 
        new_gene_entrez_mapping_dict = result_new[1] 

        final_mapping_without_entrez = EnsemblUpgradeModel.iterate_to_find_final_mapping(old_list_of_genes,old_gene_ensembl_version,new_gene_ensembl_version,force_refresh)

        assert final_mapping_without_entrez['ENSG00000115415'] == ['ENSG00000115415']
        assert final_mapping_without_entrez['ENSG00000206713'] == ['ENSG00000206713']
        assert 'ENSG00000241465' not in final_mapping_without_entrez
        assert 'ENSG00000267380' not in final_mapping_without_entrez



        assert final_mapping_without_entrez['ENSG00000283224'] == ['None']


        result = EnsemblUpgradeModel.rescue_old_genes_by_entrez(old_gene_ensembl_version,new_gene_ensembl_version,final_mapping_without_entrez,old_gene_entrez_mapping_dict,new_gene_entrez_mapping_dict)
        final_mapping_with_entrez = result[0]
        summary = result[1]
        no_mappings = summary['no_mappings']
        mappings = summary['mappings']
        rescued_with_entrez = summary['rescued_with_entrez']
        total = summary['total']

        assert final_mapping_with_entrez['ENSG00000115415'] == ['ENSG00000115415']
        assert final_mapping_with_entrez['ENSG00000206713'] == ['ENSG00000206713']
        assert 'ENSG00000241465' not in final_mapping_without_entrez
        assert 'ENSG00000267380' not in final_mapping_without_entrez




        # This is all testing from v69 to v91 human
        assert no_mappings==295 
        assert mappings==63675 
        assert total==63970 
        assert rescued_with_entrez==60 

        assert u'ENSG00000277075' in final_mapping_with_entrez['ENSG00000277075'] 
        assert u'ENSG00000276903' in final_mapping_with_entrez['ENSG00000276903']
        assert final_mapping_with_entrez['ENSG00000283224'] == ['ENSG00000065413'] # mapped using entrez



        assert 'ENSG00000007216' in final_mapping_with_entrez
        assert 'ENSG00000268478' not in final_mapping_with_entrez

        result = EnsemblUpgradeModel.dump_out_mappings(final_mapping_with_entrez,old_gene_ensembl_version,new_gene_ensembl_version)

        debug = 'False'
        force_refresh = 'no'
        summary = EnsemblUpgradeModel.get_summary_report_with_final_mapping(old_gene_ensembl_version, new_gene_ensembl_version, debug, force_refresh)

        assert summary['removed_id'] == 295
        assert summary['added_id'] == 1044
        assert summary['same_id_mapping'] == 63586
        assert summary['single_different_id_mapping'] == 46
        assert summary['multiple_id_mapping'] == 43
        assert summary['first_version_num'] == 63970
        assert summary['second_version_num'] == 64661


        return 


    @debug_on()
    def test_v67_to_v91_mouse(final_mapping):
        old_gene_ensembl_version = 'mus_musculus_core_67_37'
        new_gene_ensembl_version = 'mus_musculus_core_91_38'
        force_refresh = 'no'

        result_old = EnsemblUpgradeModel.get_full_list_of_genes(old_gene_ensembl_version,force_refresh)
        old_list_of_genes = result_old[0] 
        old_gene_entrez_mapping_dict = result_old[1] 

        result_new = EnsemblUpgradeModel.get_full_list_of_genes(new_gene_ensembl_version,force_refresh)
        new_list_of_genes = result_new[0] 
        new_gene_entrez_mapping_dict = result_new[1] 

        final_mapping_without_entrez = EnsemblUpgradeModel.iterate_to_find_final_mapping(old_list_of_genes,old_gene_ensembl_version,new_gene_ensembl_version,force_refresh)


        result = EnsemblUpgradeModel.rescue_old_genes_by_entrez(old_gene_ensembl_version,new_gene_ensembl_version,final_mapping_without_entrez,old_gene_entrez_mapping_dict,new_gene_entrez_mapping_dict)
        final_mapping_with_entrez = result[0]
        summary = result[1]
        no_mappings = summary['no_mappings']
        mappings = summary['mappings']
        rescued_with_entrez = summary['rescued_with_entrez']

        assert no_mappings==2695 
        assert mappings== 35296
        assert rescued_with_entrez==412 

        assert final_mapping_with_entrez['ENSMUSG00000078461'] == ['No mapping']
        assert final_mapping_with_entrez['ENSMUSG00000038888'] == ['ENSMUSG00000038888']
        final_mapping_with_entrez['ENSMUSG00000076161'].remove('None')
        assert len(final_mapping_with_entrez['ENSMUSG00000076161']) == 10



        result = EnsemblUpgradeModel.dump_out_mappings(final_mapping_with_entrez,old_gene_ensembl_version,new_gene_ensembl_version)
        assert result == True
        return 



if __name__ == '__main__':
    unittest.main()

    # Luxin's tests
    # tested:
    # Luxin_pass: ENSG00000000971 => ['ENSG00000000971']
    # Luxin_pass: ENSG00000006074 => ['None']
    # Luxin_failed: ENSG00000107623 => ENSG00000266524
    # there are 619 genes can be mapped from version 69 to version 86, check website https://www-ensemblb.stemformatics.org/removed_genes_final_mapping?first_version=homo_sapiens_core_69_37&second_version=homo_sapiens_core_86_38
    # search "Mysterious Disappear"
    # here are some examples for the mapping of removed genes:
    # ENSG00000108018 => ENSG00000108018
    # ENSG00000110347 => ENSG00000262406
    # ENSG00000116219 => ENSG00000268869
    #
 
