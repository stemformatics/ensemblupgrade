import re

class RegularExpression:

    def match_name_for_pseudo_gene(gene_name):
        is_pseudo = False
        regex = r"(\.[0-9]+)"
        if re.search(regex, gene_name):
            is_pseudo = True
        else:
            is_pseudo = False
        return is_pseudo

    def match_location_for_pseudo_gene(gene_location):
        is_pseudo = False
        regex = r"(chromosome) ([0-9]|1[0-9]|2[0-2]|X|Y)(: [0-9]+)(-[0-9]+)"
        if re.search(regex, gene_location):
            is_pseudo = False
        else:
            is_pseudo = True
        return is_pseudo

    def match_gene_contains_word_pseudo(gene_info):
        is_pseudo = False
        if re.search('pseudo', gene_info, re.IGNORECASE):
            is_pseudo = True
        else:
            is_pseudo = False
        return is_pseudo

    def match_gene_contains_word_hschr(gene_info):
        is_haplo = False
        if re.search('hschr', gene_info, re.IGNORECASE):
            is_haplo = True
        else:
            is_haplo = False
        return is_haplo