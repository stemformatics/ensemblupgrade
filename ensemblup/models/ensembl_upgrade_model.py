import sys, os, errno,time
import _pickle as cPickle
from pathlib import Path
import csv
from ensemblup.models.regex_defs import RegularExpression
import pymysql.cursors

#########################################################################
#########################################################################
############## Rowland's code #################
#########################################################################
#########################################################################
base_data_dir = '/home/rowlandm/ensemblupgrade/ensemblup/database/'

# ensembl_version gets rewritten later
final_mapping_pickle_file_name = base_data_dir + 'old_to_new' + '_final_mapping_dict.pkl'
"""
List of other pickle files used
full_list_pickle_file_name = base_data_dir + 'ensembl_version'+'_full_list_of_genes.pkl'
node_dict_pickle_file_name = base_data_dir + 'ensembl_version' + '_node_dict.pkl'
gene_entrez_mapping_pickle_file_name = base_data_dir + 'ensembl_version'+'_gene_entrez_mapping.pkl'
mapping_dump_file = base_data_dir + first_version + '_to_' + second_version + '_final_mapping_dict.pkl'
"""

#########################################################################
#########################################################################
#########################################################################
#########################################################################
#########################################################################


class EnsemblUpgradeModel:

    debug = 'False'
    base_data_dir = os.getcwd() + "/ensemblup/database/"

    def get_summary_report_with_final_mapping(first_version, second_version, debug, force_refresh):

        #set static attributes, which are not changed very often
        EnsemblUpgradeModel.base_data_dir = base_data_dir
        EnsemblUpgradeModel.debug = debug

        if EnsemblUpgradeModel.debug == 'True':
            EnsemblUpgradeModel.debug = True
        else:
            EnsemblUpgradeModel.debug = False
 

        summary_pickle_file_name = EnsemblUpgradeModel.base_data_dir + first_version + '_' + second_version + '_summary.pkl'

        final_mapping_pickle_file_name = EnsemblUpgradeModel.base_data_dir + first_version + '_to_' + second_version + '_final_mapping_dict.pkl'

        result_second_version = EnsemblUpgradeModel.get_full_list_of_genes(second_version,force_refresh)
        second_version_full_gene_list = result_second_version[0] 

        result_first_version = EnsemblUpgradeModel.get_full_list_of_genes(first_version,force_refresh)
        first_version_full_gene_list = result_first_version[0] 
        


        if EnsemblUpgradeModel.check_if_file_exist(final_mapping_pickle_file_name):
            output = open(final_mapping_pickle_file_name, 'rb')
            final_mapping = cPickle.load(output)
            output.close()
            single_different_list = []
            removed_list = []
            same_list = []
            added_list = []
            mapping_dict = {}
            summary_dict = {}
            full_mapping_gene_list = []

            for stable_id in final_mapping:
                for mapping_id in final_mapping[stable_id]:
                    full_mapping_gene_list.append(mapping_id)

                if final_mapping[stable_id] == ['No mapping']:
                    removed_list.append(stable_id)
                elif final_mapping[stable_id] == [stable_id]:
                    same_list.append(stable_id)
                elif len(final_mapping[stable_id]) == 1:
                    single_different_list.append(stable_id)
                elif len(final_mapping[stable_id]) > 1:
                    mapping_dict[stable_id] = final_mapping[stable_id]
                else:
                    print('error',stable_id,final_mapping[stable_id])

            # This was running slow
            #for stable_id in second_version_full_gene_list:
            #    if stable_id not in full_mapping_gene_list:
            #        added_list.append(stable_id)

            # this is way faster
            # https://stackoverflow.com/questions/15529775/missing-and-additional-values-in-2-list
            added_set = set(second_version_full_gene_list).difference(full_mapping_gene_list)
            added_list = list(added_set)

            summary_dict['removed_id'] = len(removed_list)
            summary_dict['added_id'] = len(added_list)
            summary_dict['same_id_mapping'] = len(same_list)
            summary_dict['single_different_id_mapping'] = len(single_different_list)
            summary_dict['multiple_id_mapping'] = len(mapping_dict)
            summary_dict['first_version_num'] = len(final_mapping)
            summary_dict['second_version_num'] = len(second_version_full_gene_list)

            result = [summary_dict, same_list, removed_list, added_list, mapping_dict]


        summary = result[0]
        return summary


    def check_if_file_exist(pickle_file_name):

        pickle_file_path = Path(pickle_file_name)

        if pickle_file_path.exists():
            pickle_exist = True
        else:
            pickle_exist = False

        return pickle_exist



    def show_full_genes_list_details(ensembl_version):

        pickle_file_name = EnsemblUpgradeModel.base_data_dir + ensembl_version + '_full_list_details.pkl'
        full_gene_list = []

        if EnsemblUpgradeModel.check_if_file_exist(pickle_file_name):
            output = open(pickle_file_name, 'rb')
            full_gene_dict = cPickle.load(output)
            output.close()
            for stable_id in full_gene_dict:
                full_gene_list.append(full_gene_dict[stable_id])
        else:
            print("Database Error: Please use script to generate gene full list!")

        return full_gene_list


    def get_all_removed_gene_details(first_version, second_version):
        gene_details_pickle_file_name = first_version + '_to_' + second_version + '_removed_gene_details.pkl'
        gene_details_pickle_file_name_full = EnsemblUpgradeModel.base_data_dir + gene_details_pickle_file_name
        #path_gene_details_pickle_file_name_full = Path(gene_details_pickle_file_name_full)

        if not EnsemblUpgradeModel.check_if_file_exist(gene_details_pickle_file_name_full):
            summary_pickle_file = EnsemblUpgradeModel.base_data_dir + first_version + '_' + second_version + '_summary.pkl'
            output = open(summary_pickle_file, 'rb')
            result = cPickle.load(output)
            output.close()
            removed_list = result[2]

            dict_of_removed_gene_info = EnsemblUpgradeModel.get_multiple_gene_details(first_version, removed_list)
            list_of_removed_gene_info = []

            for key in dict_of_removed_gene_info:
                gene_db_info = list(dict_of_removed_gene_info[key])
                gene_db_info.append('uncategorised')
                list_of_removed_gene_info.append(gene_db_info)

            output = open(gene_details_pickle_file_name_full, 'wb')
            cPickle.dump(list_of_removed_gene_info, output)

        return


    def get_multiple_gene_details(ensembl_version, list_of_gene_stable_ids=None):

        if list_of_gene_stable_ids is not None:
            is_full_gene_list = False
            get_partial_details_from_gene = "select stable_id, gene_id, status, biotype, seq_region_id, seq_region_start, seq_region_end, description, display_xref_id, version from gene where stable_id IN (%s)"
            gene_partial_details = EnsemblUpgradeModel.get_ensembl_database_sql(ensembl_version, get_partial_details_from_gene, list_of_gene_stable_ids)
        else:
            is_full_gene_list = True
            list_of_gene_stable_ids = []
            get_partial_details_from_gene = "select stable_id, gene_id, status, biotype, seq_region_id, seq_region_start, seq_region_end, description, display_xref_id, version from gene"
            gene_partial_details = EnsemblUpgradeModel.get_ensembl_database_sql(ensembl_version, get_partial_details_from_gene)

        gene_info_dict = {}
        xref_id_dict = {}
        seq_region_id_dict = {}
        list_of_gene_xref_ids = []
        list_of_gene_seq_region_ids = []
        list_of_gene_ids = []

        for each_gene in gene_partial_details:
            stable_id = each_gene[0]
            gene_id = each_gene[1]
            xref_id = each_gene[8]
            seq_region_id = each_gene[4]

            gene_info_dict[stable_id] = each_gene
            xref_id_dict[stable_id] = xref_id
            list_of_gene_xref_ids.append(xref_id)
            seq_region_id_dict[stable_id] = seq_region_id
            list_of_gene_seq_region_ids.append(seq_region_id)

            list_of_gene_ids.append(gene_id)
            if is_full_gene_list is True:
                list_of_gene_stable_ids.append(stable_id)

        # print(list_of_gene_ids)
        get_gene_object_xref_ids_sql = "select ensembl_id, xref_id from object_xref where ensembl_id IN (%s)"
        gene_object_xref_ids = EnsemblUpgradeModel.get_ensembl_database_sql(ensembl_version,
                                                                            get_gene_object_xref_ids_sql,
                                                                            list_of_gene_ids)

        list_of_object_xref_ids = []
        object_xref_dict = {}
        for each_gene in gene_object_xref_ids:
            ensembl_id = each_gene[0]
            object_xref_id = each_gene[1]

            object_xref_dict[object_xref_id] = ensembl_id
            list_of_object_xref_ids.append(object_xref_id)
        # every version ensembl data will use the same external_db_id 1300? I don't konw
        get_entrez_ids_sql = "select xref_id, dbprimary_acc from xref where external_db_id = '1300' and xref_id IN (%s)"
        gene_entrez_ids = EnsemblUpgradeModel.get_ensembl_database_sql(ensembl_version, get_entrez_ids_sql,
                                                                       list_of_object_xref_ids)

        gene_entrez_dict = {}
        for each_xref_id in gene_entrez_ids:
            object_xref_id = each_xref_id[0]
            entrez_id = each_xref_id[1]
            gene_id = object_xref_dict[object_xref_id]

            if gene_id not in gene_entrez_dict:
                gene_entrez_dict[gene_id] = [entrez_id]
            else:
                gene_entrez_dict[gene_id].append(entrez_id)

        #        for key in gene_entrez_dict:
        #            if len(gene_entrez_dict[key]) > 1:
        #                gene_entrez_dict[key].append('multiple_entrez')

        get_gene_name_sql = "select xref_id, display_label from xref where xref_id IN (%s)"
        gene_names = EnsemblUpgradeModel.get_ensembl_database_sql(ensembl_version, get_gene_name_sql,
                                                                  list_of_gene_xref_ids)
        gene_names_dict = {}

        for each_gene in gene_names:
            xref_id = each_gene[0]
            display_label = each_gene[1]

            gene_names_dict[xref_id] = display_label

        get_gene_seq_details = "select * from seq_region where seq_region_id IN (%s)"
        gene_seq_details = EnsemblUpgradeModel.get_ensembl_database_sql(ensembl_version, get_gene_seq_details,
                                                                        list_of_gene_seq_region_ids)
        gene_seq_details_dict = {}
        list_of_coord_system_ids = []
        coord_system_id_dict = {}

        for each_seq in gene_seq_details:
            seq_region_id = each_seq[0]
            name = each_seq[1]
            coord_system_id = each_seq[2]

            list_of_coord_system_ids.append(coord_system_id)
            coord_system_id_dict[seq_region_id] = coord_system_id

            gene_seq_details_dict[seq_region_id] = name

        get_gene_location_type = "select coord_system_id, name from coord_system where coord_system_id IN (%s)"
        gene_location_type = EnsemblUpgradeModel.get_ensembl_database_sql(ensembl_version, get_gene_location_type,
                                                                          list_of_coord_system_ids)
        gene_location_type_dict = {}
        for each_coord in gene_location_type:
            coord_system_id = each_coord[0]
            name = each_coord[1]

            gene_location_type_dict[coord_system_id] = name

        gene_full_info_dict = {}
        for each_stable_id in list_of_gene_stable_ids:
            the_gene_info = gene_info_dict[each_stable_id]
            the_xref_id = xref_id_dict[each_stable_id]
            the_display_label = gene_names_dict[the_xref_id]
            the_seq_region_id = seq_region_id_dict[each_stable_id]
            the_seq_name = gene_seq_details_dict[the_seq_region_id]
            the_coord_id = coord_system_id_dict[the_seq_region_id]
            the_coord_name = gene_location_type_dict[the_coord_id]
            the_gene_location = the_coord_name + " " + the_seq_name + ": " + str(the_gene_info[5]) + "-" + str(
                the_gene_info[6])
            the_gene_description = the_gene_info[7]
            the_gene_type = the_gene_info[2] + " " + the_gene_info[3]
            the_gene_version = the_gene_info[9]
            gene_id = the_gene_info[1]
            if gene_id in gene_entrez_dict:
                the_gene_entrez = gene_entrez_dict[gene_id]
            else:
                the_gene_entrez = None
            the_gene_full_info = [each_stable_id, the_display_label, the_gene_version, the_gene_description, the_gene_location,
                                  the_gene_type, the_gene_entrez]
            gene_full_info_dict[each_stable_id] = the_gene_full_info
        return gene_full_info_dict


    def get_ensembl_database_sql(ensembl_version, sql, list_of_gene_stable_ids=None):

        import pymysql.cursors
        connection = pymysql.connect(host="ensembldb.ensembl.org",
                                     user="anonymous",
                                     port=3306,
                                     password="",
                                     db= ensembl_version)

        with connection.cursor() as cursor:
            # Create a new record

            if list_of_gene_stable_ids is not None:
                format_strings = ','.join(['%s'] * len(list_of_gene_stable_ids))
                cursor.execute(sql % format_strings, tuple(list_of_gene_stable_ids))
            else:
                cursor.execute(sql, )

            result = cursor.fetchall()
            return result


    def categorise_removed_genes(first_version, second_version):

        pickle_file_full_name = EnsemblUpgradeModel.base_data_dir + first_version + '_to_' + second_version + '_removed_gene_details.pkl'
        output = open(pickle_file_full_name, 'rb')
        result = cPickle.load(output)
        output.close()

        uncategorised_gene_num = 0
        pseudo_gene_desc_num = 0
        pseudo_gene_type_num = 0
        haplo_type_gene_num = 0
        entrez_mapping_num = 0

        removed_gene_name_list = []
        first_to_second_stable_ids = EnsemblUpgradeModel.map_entrez_ids_for_second_version(first_version, second_version)
        #removed_entrez_mapping = []
        #removed_entrez_mapping.append(['version69', 'version86'])

        for gene in result:
            stable_id = gene[0]
            gene_name = gene[1]
            gene_version = gene[2]
            gene_description = gene[3]
            gene_location = gene[4]
            gene_type = gene[5]
            gene_entrez_id = gene[6]
            gene_categorised_info = []

            if stable_id in first_to_second_stable_ids:
                gene_categorised_info = first_to_second_stable_ids[stable_id]
                entrez_mapping_num = entrez_mapping_num + 1
                #for item in first_to_second_stable_ids[stable_id]:
                    #removed_entrez_mapping.append([stable_id, item])
                    #uncategorised_gene_num = uncategorised_gene_num - 1
            if RegularExpression.match_gene_contains_word_hschr(gene_location):
                gene_categorised_info.append("haplotype")
                haplo_type_gene_num = haplo_type_gene_num + 1
                #uncategorised_gene_num = uncategorised_gene_num - 1

            if not gene_categorised_info:
                uncategorised_gene_num = uncategorised_gene_num + 1
                gene_categorised_info.append('uncategorised')

            if gene_entrez_id is not None:
                if len(gene_entrez_id) > 1:
                    gene_entrez_id.append('multiple_entrez')

            removed_gene_name_list.append((stable_id, gene_name, gene_version, gene_location, gene_description, gene_type, gene_entrez_id, gene_categorised_info))

        #with open('csv_file_name.tsv', 'w', newline="") as f:
            #writer = csv.writer(f, delimiter='\t')
            #writer.writerows(removed_entrez_mapping)
        first_version_full_list = EnsemblUpgradeModel.base_data_dir + first_version + '_full_list_of_genes.pkl'
        first_version_output = open(first_version_full_list, 'rb')
        first_version_result = cPickle.load(first_version_output)
        first_version_output.close()

        total_num = len(first_version_result)
        uncategorised_percent = uncategorised_gene_num / total_num
        uncategorised_percent_format = "{:.1%}".format(uncategorised_percent)
        return removed_gene_name_list,pseudo_gene_desc_num,pseudo_gene_type_num, uncategorised_gene_num, haplo_type_gene_num,\
               entrez_mapping_num, uncategorised_percent_format, total_num


    def map_entrez_ids_for_second_version(first_version, second_version):
        gene_details_pickle_file_name = first_version + '_to_' + second_version + '_removed_gene_details.pkl'
        gene_details_pickle_file_name_full = EnsemblUpgradeModel.base_data_dir + gene_details_pickle_file_name

        if EnsemblUpgradeModel.check_if_file_exist(gene_details_pickle_file_name_full):
            output = open(gene_details_pickle_file_name_full, 'rb')
            list_of_removed_gene_info = cPickle.load(output)
            output.close()

            entrez_id_dict = {}
            entrez_id_list = []
            for each_gene in list_of_removed_gene_info:
                the_stable_id = each_gene[0]
                the_entrez_id_list = each_gene[6]
                if the_entrez_id_list is not None:
                    for entrez_id in the_entrez_id_list:
                        if entrez_id not in entrez_id_dict:
                            entrez_id_dict[entrez_id] = [the_stable_id]
                            entrez_id_list.append(entrez_id)
                        else:
                            entrez_id_dict[entrez_id].append(the_stable_id)


            get_xref_ids_sql = "select xref_id, dbprimary_acc from xref where external_db_id = '1300' and dbprimary_acc IN (%s)"
            gene_xref_ids = EnsemblUpgradeModel.get_ensembl_database_sql(second_version, get_xref_ids_sql, entrez_id_list)

            gene_xref_dict = {}
            gene_xref_list = []
            for each_xref_id in gene_xref_ids:
                object_xref_id = each_xref_id[0]
                entrez_id = each_xref_id[1]

                if object_xref_id not in gene_xref_dict:
                    gene_xref_dict[object_xref_id] = [entrez_id]
                    gene_xref_list.append(object_xref_id)
                else:
                    gene_xref_dict[object_xref_id].append(entrez_id)


            get_gene_ensembl_ids_sql = "select ensembl_id, xref_id from object_xref where xref_id IN (%s)"
            gene_ensembl_ids = EnsemblUpgradeModel.get_ensembl_database_sql(second_version, get_gene_ensembl_ids_sql,
                                                                            gene_xref_list)

            ensembl_xref_dict = {}
            ensembl_id_list = []
            for each_ensembl_xref in gene_ensembl_ids:
                ensembl_id = each_ensembl_xref[0]
                xref_id = each_ensembl_xref[1]

                if ensembl_id not in ensembl_xref_dict:
                    ensembl_xref_dict[ensembl_id] = [xref_id]
                    ensembl_id_list.append(ensembl_id)
                else:
                    ensembl_xref_dict[ensembl_id].append(xref_id)

            # huge problem , consider the multiple mapping
            get_stable_id_sql = "select stable_id, gene_id from gene where gene_id IN (%s)"
            gene_stable_ids = EnsemblUpgradeModel.get_ensembl_database_sql(second_version, get_stable_id_sql, ensembl_id_list)

            old_to_new_stable_ids_mapping = {}
            new_to_old_stable_ids_mapping = {}

            for each_gene in gene_stable_ids:
                stable_id = each_gene[0]
                gene_id = each_gene[1]

                # ensembl_id_stable_id_dict[gene_id] = stable_id
                the_xref_id_list = ensembl_xref_dict[gene_id]
                for the_xref_id in the_xref_id_list:
                    the_entrez_id_list = gene_xref_dict[the_xref_id]
                    for the_entrez_id in the_entrez_id_list:
                        the_old_stable_id_list = entrez_id_dict[the_entrez_id]
                        for the_old_stable_id in the_old_stable_id_list:
                            if the_old_stable_id not in new_to_old_stable_ids_mapping:
                                new_to_old_stable_ids_mapping[the_old_stable_id] = [stable_id]
                            else:
                                new_to_old_stable_ids_mapping[the_old_stable_id].append(stable_id)

                #old_to_new_stable_ids_mapping[stable_id] = the_old_stable_id
                #new_to_old_stable_ids_mapping[the_old_stable_id] = stable_id
            for stable_id in new_to_old_stable_ids_mapping:
                new_to_old_stable_ids_mapping[stable_id].append("Entrez M")
            return new_to_old_stable_ids_mapping


    def get_all_added_gene_details(first_version, second_version):
        gene_details_pickle_file_name = first_version + '_to_' + second_version + '_added_gene_details.pkl'
        gene_details_pickle_file_name_full = EnsemblUpgradeModel.base_data_dir + gene_details_pickle_file_name

        if EnsemblUpgradeModel.check_if_file_exist(gene_details_pickle_file_name_full):
            output = open(gene_details_pickle_file_name_full, 'rb')
            list_of_added_gene_info = cPickle.load(output)
            output.close()
            return list_of_added_gene_info
        else:

            summary_pickle_file = EnsemblUpgradeModel.base_data_dir + first_version + '_' + second_version + '_summary.pkl'
            output = open(summary_pickle_file, 'rb')
            result = cPickle.load(output)
            output.close()
            added_list = result[3]

            dict_of_added_gene_info = EnsemblUpgradeModel.get_multiple_gene_details(second_version, added_list)
            list_of_added_gene_info = []

            for key in dict_of_added_gene_info:
                gene_db_info = list(dict_of_added_gene_info[key])
                gene_db_info.append('uncategorised')
                list_of_added_gene_info.append(gene_db_info)

            output = open(gene_details_pickle_file_name_full, 'wb')
            cPickle.dump(list_of_added_gene_info, output)
            output.close()
            return list_of_added_gene_info


    def get_mapping_history(first_version, second_version):
        mapping_pickle_file = EnsemblUpgradeModel.base_data_dir + first_version + '_' + second_version + '_full_mapping_history.pkl'
        if EnsemblUpgradeModel.check_if_file_exist(mapping_pickle_file):

            # Pickle dictionary using protocol 0.
            output = open(mapping_pickle_file, 'rb')
            result = cPickle.load(output)
            output.close()
            new_result = []

            for row in result:
                if row[0] != None and row[1] != None:
                    new_result.append(row)

        else:
            new_result = []

        return new_result


    def get_genes_mapped_to_other_genes(first_version, second_version):

        summary_pickle_file = EnsemblUpgradeModel.base_data_dir + first_version + '_' + second_version + '_summary.pkl'
        output = open(summary_pickle_file, 'rb')
        result = cPickle.load(output)
        output.close()
        mapping_dict = result[4]
        mapping_list = []

        for key in mapping_dict:
            mapping_list.append([key, mapping_dict[key]])

        return mapping_list

    def get_full_final_mapping(first_version, second_version):

        full_final_mapping_pkl = EnsemblUpgradeModel.base_data_dir + 'full_final_mapping_' + first_version + '_' + second_version + '.pkl'
        output = open(full_final_mapping_pkl, 'rb')
        result = cPickle.load(output)
        output.close()
        mapping_dict = result
        mapping_list = []

        for key in mapping_dict:
            mapping_list.append([key, mapping_dict[key]])

        return mapping_list


    def removed_genes_final_mapping(first_version, second_version):

        summary_pickle_file = EnsemblUpgradeModel.base_data_dir + first_version + '_' + second_version + '_removed_mapping.pkl'
        output = open(summary_pickle_file, 'rb')
        removed_mapping_dict = cPickle.load(output)
        output.close()
        mapping_session_pickle_file = EnsemblUpgradeModel.base_data_dir + first_version + '_' + second_version + '_mapping_session.pkl'
        if EnsemblUpgradeModel.check_if_file_exist(mapping_session_pickle_file):
            output = open(mapping_session_pickle_file, 'rb')
            mapping_session_list = cPickle.load(output)
            output.close()
        else:
            first_version_mapping_session_list_sql = "select DISTINCT mapping_session_id from stable_id_event"
            first_version_mapping_session_tuple = EnsemblUpgradeModel.get_ensembl_database_sql(first_version,first_version_mapping_session_list_sql)

            first_version_mapping_session_list = []

            for item in first_version_mapping_session_tuple:
                first_version_mapping_session_list.append(item[0])

            mapping_session_between_two_versions_sql = "select DISTINCT mapping_session_id from stable_id_event where mapping_session_id NOT IN (%s)"
            mapping_session_between_two_versions = EnsemblUpgradeModel.get_ensembl_database_sql(second_version, mapping_session_between_two_versions_sql,first_version_mapping_session_list)


            mapping_session_list_between_versions = []

            for item in mapping_session_between_two_versions:
                mapping_session_list_between_versions.append(item[0])

            mapping_session_list_sql = "select mapping_session_id, old_db_name, new_db_name, old_assembly, new_assembly from mapping_session where mapping_session_id IN (%s)"
            mapping_session_list = EnsemblUpgradeModel.get_ensembl_database_sql(second_version, mapping_session_list_sql,mapping_session_list_between_versions)

            output = open(mapping_session_pickle_file, 'wb')
            cPickle.dump(mapping_session_list, output)
            output.close()

        mapping_session_dict = {}
        for each_mapping_session in mapping_session_list:
            mapping_session_dict[each_mapping_session[0]] = each_mapping_session

        removed_genes_final_mapping_dict = {}

        for the_removed_gene in removed_mapping_dict:
            the_remove_mapping = removed_mapping_dict[the_removed_gene]
            the_mapping_session_id = 0
            for the_mapping in the_remove_mapping:
                if the_mapping[2] != "Mysterious Disappear!":
                    if the_mapping[2]>the_mapping_session_id:
                        the_mapping_session_id = the_mapping[2]
            if the_mapping_session_id == 0:
                mapping_session_info = None
                mapping_session_display = None
            else:
                mapping_session_info = mapping_session_dict[the_mapping_session_id]
                mapping_session_display = mapping_session_info[1]+" to " +mapping_session_info[2]
            removed_genes_final_mapping_dict[the_removed_gene] = (mapping_session_display, the_remove_mapping)

        removed_genes_final_mapping_list = []
        for key in removed_genes_final_mapping_dict:
            removed_genes_final_mapping_list.append([key, removed_genes_final_mapping_dict[key][0], removed_genes_final_mapping_dict[key][1]])

        return removed_genes_final_mapping_list


        return []

    ###############################################################
    ###############################################################
    ###############################################################
    ################ Rowland's code from April 2018 ###############
    ###############################################################
    ###############################################################
    ###############################################################

    def get_full_list_of_genes(ensembl_version,force_refresh):
        all_info_for_genes_by_gene_id = {}
        full_list_pickle_file_name = base_data_dir + ensembl_version+'_full_list_of_genes.pkl'
        pickle_file_name = full_list_pickle_file_name
        temp_force_refresh = force_refresh
        if temp_force_refresh =='no' :

            try:
                # Pickle dictionary using protocol 0.
                output = open(pickle_file_name, 'rb')
                result = cPickle.load(output)
                output.close()
            except:
                temp_force_refresh = 'yes'


        if temp_force_refresh == 'yes':
            connection = pymysql.connect(host="ensembldb.ensembl.org",
                                         user="anonymous",
                                         port=3306,
                                         password="",
                                         db=ensembl_version)

            with connection.cursor() as cursor:
                # Create a new record

                sql ="select gene_id, stable_id, version from gene;"

                cursor.execute(sql,)
                result = cursor.fetchall()


            output = open(pickle_file_name, 'wb')
            cPickle.dump(result, output)
            output.close()

        print('This now has the data for ' + ensembl_version)

        full_gene_ids = []
        full_stable_ids = []
        for temp_array in result:
            gene_id =  int(temp_array[0])
            stable_id = temp_array[1] # This is like a real ensembl ID eg.  ENSG00000115415
            version = int(temp_array[2])
            all_info_for_genes_by_gene_id[gene_id] = {'gene_id':gene_id,'stable_id':stable_id,'version':version}
            full_stable_ids.append(stable_id)
            full_gene_ids.append(gene_id)


        print('This now has finished the first part of the full_stable_ids')

        gene_ids_to_entrez_id_mapping = EnsemblUpgradeModel.get_all_entrez_ids(ensembl_version,full_gene_ids,all_info_for_genes_by_gene_id,force_refresh)

        print('This now has finished the second part of the full_stable_ids')


        return [full_stable_ids,gene_ids_to_entrez_id_mapping]



    def get_all_entrez_ids(ensembl_version,list_of_all_gene_ids,all_info_for_genes_by_gene_id,force_refresh):

        temp_force_refresh = force_refresh
        if temp_force_refresh =='no' :

            try:

                gene_entrez_mapping_pickle_file_name = base_data_dir + ensembl_version+'_gene_entrez_mapping.pkl'

                output = open(gene_entrez_mapping_pickle_file_name, 'rb')
                gene_entrez_dict = cPickle.load(output)
                output.close()
            except:
                temp_force_refresh = 'yes'

        if temp_force_refresh == 'yes':
            gene_entrez_dict = {}
     
            print("starting entrez")
            start = time.time()
            ###################################################################3
            # Adding Entrez -start
            # Now also get the Entrez ids for everything
            ###################################################################3


            get_gene_object_xref_ids_sql = "select ensembl_id, xref_id from object_xref where ensembl_id IN (%s)"
            gene_object_xref_ids = EnsemblUpgradeModel.get_ensembl_database_sql(ensembl_version, get_gene_object_xref_ids_sql, list_of_all_gene_ids)
            print("finishing object_xref in  entrez")
            end = time.time()
            print(end - start)

            list_of_object_xref_ids = []
            object_xref_dict = {}
            for each_gene in gene_object_xref_ids:
                ensembl_id = each_gene[0]
                xref_id = each_gene[1]

                if xref_id not in object_xref_dict:
                    object_xref_dict[xref_id] = []
                    
                object_xref_dict[xref_id].append(ensembl_id)
                list_of_object_xref_ids.append(xref_id)

           
            start = time.time()

            # every version ensembl data will use the same external_db_id 1300? I don't know
            get_entrez_ids_sql = "select xref_id, dbprimary_acc from xref where external_db_id = '1300' and xref_id IN (%s)"
            gene_entrez_ids = EnsemblUpgradeModel.get_ensembl_database_sql(ensembl_version, get_entrez_ids_sql, list_of_object_xref_ids)

            print("finishing xref in  entrez")
            end = time.time()
            print(end - start)

            for each_xref_id in gene_entrez_ids:
                object_xref_id = each_xref_id[0]
                entrez_id = int(each_xref_id[1])
            
                for gene_id in object_xref_dict[object_xref_id]:

                    stable_id = all_info_for_genes_by_gene_id[gene_id]['stable_id']
                    if stable_id not in gene_entrez_dict:
                        gene_entrez_dict[stable_id] = [entrez_id]
                    else:
                        gene_entrez_dict[stable_id].append(entrez_id)


            gene_entrez_mapping_pickle_file_name = base_data_dir + ensembl_version+'_gene_entrez_mapping.pkl'
            output = open(gene_entrez_mapping_pickle_file_name, 'wb')
            cPickle.dump(gene_entrez_dict, output)
            output.close()

        return gene_entrez_dict 



    def iterate_to_find_final_mapping(list_of_original_genes,old_gene_ensembl_version,new_gene_ensembl_version,force_refresh):
        node_dict_pickle_file_name = base_data_dir + new_gene_ensembl_version + '_node_dict.pkl'
        temp_force_refresh = force_refresh
        if temp_force_refresh =='no' :

            try:

                pickle_file_name = node_dict_pickle_file_name

                output = open(pickle_file_name, 'rb')
                node_dict = cPickle.load(output)
                output.close()
            except:
                temp_force_refresh = 'yes'

        if temp_force_refresh == 'yes':
            # now need to iterate over this:
            start= time.time()
            map_to_new = {}
            list_of_original_genes = list(set(list_of_original_genes))


            # eg. homo_sapiens_core_70_37
            # split on 'core_' so you end up with
            # ['homo_sapiens_','70_37']
            # take the second element ie. [1] which is '70_37'
            # split on '_' so you get ['70','37'] and then choose '70'
            release_id = int(old_gene_ensembl_version.split('core_')[1].split('_')[0])

            print("About to call sql with release_id " + str(release_id))

            sql = "select mapping_session_id,old_release,new_release from mapping_session where old_release = %(release_id)s;"
            connection = pymysql.connect(host="ensembldb.ensembl.org",
                                         user="anonymous",
                                         port=3306,
                                         password="",
                                         db=new_gene_ensembl_version)

            with connection.cursor() as cursor:
                cursor.execute(sql,  {"release_id":release_id})
                
                result = cursor.fetchall()

            try:
                mapping_session_id = result[0][0]
            except:
                print("Error calculating mapping_session_id. Erroring out.")
                return 

            end = time.time()
            time_taken = end - start
            print('find mapping_session_id time_taken in seconds is: ' + str(time_taken))
            start = end

            node_dict = {}

            print("================")
            print("About to call sql with mapping_session_id " + str(mapping_session_id))
            sql = "select * from stable_id_event where (old_stable_id != new_stable_id or new_stable_id is NULL) and mapping_session_id >= %(mapping_session_id)s order by mapping_session_id,old_stable_id"
            connection = pymysql.connect(host="ensembldb.ensembl.org",
                                         user="anonymous",
                                         port=3306,
                                         password="",
                                         db=new_gene_ensembl_version)

            with connection.cursor() as cursor:
                cursor.execute(sql,  {"mapping_session_id":mapping_session_id})
                
                result = cursor.fetchall()

            print("number of stable_id_events from sql " +str(len(result)))

            end = time.time()
            time_taken = end - start
            print('time_taken in seconds is: ' + str(time_taken))
            start = end

            new_map_count = 0
            new_none_count = 0
            full_count = 0
            same_gene_count = 0

            for row in result:
                temp_old_state_id = row[0]
                temp_new_state_id = row[2]
                full_count += 1


                if temp_new_state_id is None:
                    temp_new_state_id = 'None'
                    if temp_old_state_id not in map_to_new:
                        map_to_new[temp_old_state_id] = []

                    map_to_new[temp_old_state_id].append(temp_new_state_id)

                    new_none_count += 1

                    if temp_old_state_id not in node_dict:
                        node_dict[temp_old_state_id] = Node(temp_old_state_id,mapping_session_id)

                    temp_parent_node = node_dict[temp_old_state_id]

                    temp_child_node = Node(temp_new_state_id,mapping_session_id)

                    temp_parent_node.add_child(temp_child_node)

                    node_dict[temp_old_state_id] = temp_parent_node

                else:

                    if temp_old_state_id not in node_dict:
                        node_dict[temp_old_state_id] = Node(temp_old_state_id,mapping_session_id)

                    if temp_new_state_id not in node_dict:
                        node_dict[temp_new_state_id] = Node(temp_new_state_id,mapping_session_id)

                    temp_parent_node = node_dict[temp_old_state_id]
                    temp_child_node = node_dict[temp_new_state_id]

                    temp_parent_node.add_child(temp_child_node)
                    node_dict[temp_old_state_id] = temp_parent_node

                    temp_child_node.add_parent(temp_old_state_id)
                    node_dict[temp_new_state_id] = temp_child_node



                    if temp_old_state_id == temp_new_state_id:
                        same_gene_count += 1
                    if temp_old_state_id != temp_new_state_id:

                        if temp_old_state_id not in map_to_new:
                            map_to_new[temp_old_state_id] = []
                        map_to_new[temp_old_state_id].append(temp_new_state_id)

                        new_map_count += 1



            end = time.time()
            time_taken = end - start
            print('Node setup time_taken in seconds is: ' + str(time_taken))

            pickle_file_name = node_dict_pickle_file_name
            output = open(pickle_file_name, 'wb')
            cPickle.dump(node_dict, output)
            output.close()

        final_mapping = {}

        start = time.time()
        for gene_id in list_of_original_genes:
            # assume that the gene maps to the other gene
            flattened_list = [gene_id]

            if gene_id in node_dict:
                flattened_list = EnsemblUpgradeModel.traverse(node_dict, gene_id, flattened_list)

            final_mapping[gene_id] = list(set(flattened_list))

        end = time.time()
        time_taken = end - start
        print('Mapping time time_taken in seconds is: ' + str(time_taken))

        print("This has done the final mapping, without entrez mapping")

        final_mapping_pickle_file_name = base_data_dir + old_gene_ensembl_version + '_to_' + new_gene_ensembl_version + '_final_mapping_dict.pkl'
        pickle_file_name = final_mapping_pickle_file_name
        output = open(pickle_file_name, 'wb')
        cPickle.dump(final_mapping, output)
        output.close()

        return final_mapping


    # http://stackoverflow.com/questions/6247751/python-tree-traversal-question
    # http://stackoverflow.com/questions/231767/what-does-the-yield-keyword-do-in-python
    def traverse(node_dict,gene_id,flattened_list):
        """
            Peform callback on all nodes in depth-first order
            e.g. traverse(root, lambda x:print(x))
        """
        if gene_id == 'None':
            return flattened_list

        try:
            root = node_dict[gene_id]
        except:
            pass

        # remove gene_id and replace it with children
        if len(root.children) != 0:
            try:
                flattened_list.remove(gene_id)
            except:
                pass

        for child in root.children:
            child_gene_id = child.gene_id


            if child_gene_id not in flattened_list:
                flattened_list.append(child_gene_id)

            # eg. ENSG00000207103 points to ENSG00000253437 and vice versa
            # Still keep them, just don't recurse down the tree again
            if child_gene_id not in root.parents:
                flattened_list = EnsemblUpgradeModel.traverse(node_dict,child_gene_id,flattened_list)

        return flattened_list

    def dump_out_mappings(mapping,old_gene_ensembl_version,new_gene_ensembl_version):
        mapping_dump_file = base_data_dir + old_gene_ensembl_version + '_to_' + new_gene_ensembl_version + '_final_mapping.tsv'
        output = open(mapping_dump_file, 'w')

        for old_stable_id in mapping:
            list_of_new_stable_ids = mapping[old_stable_id]

            for new_stable_id in list_of_new_stable_ids:
                if new_stable_id == 'No mapping':
                    new_stable_id = ''

                if new_stable_id == 'None':
                    continue
                line_of_output= old_stable_id + "\t" + new_stable_id+"\n"
                output.write(line_of_output)

        output.close()
        return True

    def get_list_of_previous_mappings():

        list_of_mappings = []

        for file in os.listdir(base_data_dir):
            if file.endswith("final_mapping.tsv"):
                # starts with homo_sapiens_core_69_37_to_homo_sapiens_core_91_38_final_mapping.tsv
                # split on _to_ for this ["homo_sapiens_core_69_37","homo_sapiens_core_91_38_final_mapping.tsv"]
                # split on _final_mapping.tsv for the last item in the list

                temp_list = file.split("_to_")
                from_ensembl_version = temp_list[0]
                temp_list = temp_list[1].split("_final_mapping.tsv")
                to_ensembl_version = temp_list[0]
                
                print(from_ensembl_version,to_ensembl_version,file) 
                list_of_mappings.append([from_ensembl_version,to_ensembl_version]) 

        return list_of_mappings

    def rescue_old_genes_by_entrez(old_gene_ensembl_version,new_gene_ensembl_version,final_mapping_without_entrez,old_gene_entrez_mapping_dict,new_gene_entrez_mapping_dict):

        no_mappings = 0
        mappings = 0
        rescued_with_entrez = 0

        # Invert so that when we find the entrez_id in the old version
        # we can easily find the entrez_id in the new version.
        entrez_to_gene_for_new_version_dict = {}
        for stable_id,list_of_entrez_ids in new_gene_entrez_mapping_dict.items():
            for entrez_id in list_of_entrez_ids:
                if entrez_id not in entrez_to_gene_for_new_version_dict:
                    entrez_to_gene_for_new_version_dict[entrez_id] = []
                entrez_to_gene_for_new_version_dict[entrez_id].append(stable_id)

        final_mapping_with_entrez = {}

        for stable_id in final_mapping_without_entrez:

            list_of_genes_mapped_to = final_mapping_without_entrez[stable_id]
            if list_of_genes_mapped_to == ['None'] or list_of_genes_mapped_to == []:

                mapped_this_loop = False

                if stable_id in old_gene_entrez_mapping_dict:
                    entrez_ids_for_this_gene = old_gene_entrez_mapping_dict[stable_id]
                    if len(entrez_ids_for_this_gene) > 0:

                        for entrez_id in entrez_ids_for_this_gene:
                            try:
                                genes_found = entrez_to_gene_for_new_version_dict[entrez_id]
                                for mapped_stable_id in genes_found:
                                    if stable_id not in final_mapping_with_entrez:
                                        final_mapping_with_entrez[stable_id] = []
                                    if mapped_stable_id not in final_mapping_with_entrez[stable_id]:
                                        final_mapping_with_entrez[stable_id].append(mapped_stable_id)
                                        mapped_this_loop = True

                            except:
                                continue 


                
                if mapped_this_loop:
                    mappings+=1
                    rescued_with_entrez+=1
                    #print('rescued ',stable_id)
                else:
                    no_mappings+=1
                    final_mapping_with_entrez[stable_id] = ['No mapping']

            else:   
                final_mapping_with_entrez[stable_id] = final_mapping_without_entrez[stable_id]
                mappings += 1 
        summary = {'mappings':mappings,'no_mappings':no_mappings,'rescued_with_entrez':rescued_with_entrez,'total':len(final_mapping_without_entrez)}


        final_mapping_pickle_file_name = base_data_dir + old_gene_ensembl_version + '_to_' + new_gene_ensembl_version + '_final_mapping_dict.pkl'
        pickle_file_name = final_mapping_pickle_file_name
        output = open(pickle_file_name, 'wb')
        cPickle.dump(final_mapping_with_entrez, output)
        output.close()


        return [final_mapping_with_entrez,summary]


class Node(object):
    def __init__(self, gene_id,mapping_session_id):
        self.gene_id = gene_id
        self.mapping_session_id = mapping_session_id
        self.children = []
        self.parents = []

    def add_child(self, obj):
        self.children.append(obj)
    
    def add_parent(self,gene_id):
        self.parents.append(gene_id)

