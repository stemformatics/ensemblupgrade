from pyramid.config import Configurator
from ensemblup.controllers.ensembl_upgrade_controller import EnsemblUpgradeController
from pyramid_handlers import action
from pyramid.view import view_config
from ensemblup.config import *


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings)
    config.include('pyramid_mako')
    config.include('pyramid_handlers')
    config.add_static_view('static', 'static', cache_max_age=3600)

    config.add_handler('home', '/', handler = EnsemblUpgradeController, action='home_page')
    config.add_handler('generate_summary_report','/generate_summary_report',
                       handler = EnsemblUpgradeController, action = 'generate_summary_report')
    config.add_handler('get_removed_gene_details','/get_removed_gene_details',
                       handler = EnsemblUpgradeController, action = 'get_removed_gene_details')
    config.add_handler('get_added_gene_details','/get_added_gene_details',
                       handler = EnsemblUpgradeController, action = 'get_added_gene_details')
    config.add_handler('map_all_removed_genes','/map_all_removed_genes',
                       handler = EnsemblUpgradeController, action = 'map_all_removed_genes')
    config.add_handler('show_full_genes_list_details','/show_full_genes_list_details',
                       handler = EnsemblUpgradeController, action = 'show_full_genes_list_details')
    config.add_handler('mapping_history','/mapping_history',
                       handler = EnsemblUpgradeController, action = 'mapping_history')
    config.add_handler('generate_summary_report_with_final_mapping','/generate_summary_report_with_final_mapping',
                       handler = EnsemblUpgradeController, action = 'generate_summary_report_with_final_mapping')
    config.add_handler('genes_mapped_to_other_genes','/genes_mapped_to_other_genes',
                       handler = EnsemblUpgradeController, action = 'genes_mapped_to_other_genes')
    config.add_handler('removed_genes_final_mapping','/removed_genes_final_mapping',
                       handler = EnsemblUpgradeController, action = 'removed_genes_final_mapping')
    config.add_handler('full_final_mapping','/full_final_mapping',
                       handler = EnsemblUpgradeController, action = 'full_final_mapping')
    config.scan()
    return config.make_wsgi_app()


