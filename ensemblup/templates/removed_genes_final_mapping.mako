<%inherit file="base.mako"/>
<%block name="partial_content">
<h2>${first_version} to ${second_version}</h2>
<br><br>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    {
                        text: 'TSV',
                        extend: 'csvHtml5',
                        fieldSeparator: '\t',
                        extension: '.tsv'
                    }
                ]
            } );
        } );
    </script>
    <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Removed Stable Id</th>
                <th>Stop Mapping Session</th>
                <th>Stop Mapping List</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Removed Stable Id</th>
                <th>Stop Mapping Session</th>
                <th>Stop Mapping List</th>
            </tr>
        </tfoot>
        <tbody>
            % for exp in examples:
            <tr>
                <% old_id = exp[0] %> <td>${old_id}</td>
                <% mapping_session = exp[1] %> <td>${mapping_session}</td>
                <% mapping_list = exp[2] %> <td>${mapping_list}</td>

            </tr>
            % endfor
        </tbody>>
    </table>


</%block>