<div class="footer">

    <div class="footer-panel">
        <div class="left-text">
            <span>© 2018 ${web_info['site_name']}</span>
            <span class="divider">|</span>
            <span>${web_info['ensembl_upgrade_version']}</span>
        </div>
        <div class="right-text">
            <div class="licenses">
                <span class"footer-tag"> Data </span>
                <a target="_blank" href="https://creativecommons.org/licenses/by-nd/4.0/"> CC BY-SA 4.0 </a>
                <span class="divider">|</span>
                <span class"footer-tag"> Code </span>
                <a target="_blank" href="https://bitbucket.org/stemformatics/ensemblupgrade/overview"> Apache 2.0 </a>
            </div>
            <div class="hosts">
                <span class"footer-tag"> Website hosted by </span>
                <a class="nectar" href="http://nectar.org.au"></a>
            </div>
        </div>
    </div>
</div>
