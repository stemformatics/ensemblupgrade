<%inherit file="base.mako"/>
<%block name="partial_content">
    <h2>Summary Report</h2>
    <h3>${first_version} to ${second_version}</h3><br>
    <form action="/show_full_genes_list_details" method="get">
        <div hidden><input value="${first_version}" type="text" name="version" size = "35" readonly="true"></div>
        <button type="submit">${first_version} Full Genes Details</button>
    </form>

    <form action="/show_full_genes_list_details" method="get">
        <div hidden><input value="${second_version}" type="text" name="version" size = "35" readonly="true"></div>
        <button type="submit">${second_version} Full Genes Details</button>
    </form>
    <form action="/mapping_history" method="get">
        <div hidden><input value="${first_version}" type="text" name="first_version" size = "35" readonly="true"></div>
        <div hidden><input value="${second_version}" type="text" name="second_version" size = "35" readonly="true"></div>
        <button type="submit">View All Mapping History</button>
    </form>

    <b>Genes with Same id and Same Version:&emsp;&emsp;&nbsp;${summary['same_id_same_version']} </b><br>
    <b>Genes maped to multiple genes:&emsp;${summary['same_id_diff_version']} </b><br>
    <b>Genes Removed from ${second_version}:&emsp;&emsp;&nbsp;${summary['removed_id']} </b><br>
    <b>Genes Added in${second_version}&emsp;&emsp;&nbsp;${summary['added_id']} </b><br>

    <br><br><br><br>

    <table width="70%"  border="1">
        <tr>
            <th height="40">Options for Added Genes</th>
            <th height="40">Options for Removed Genes</th>
        </tr>

        <tr>
            <td height="40">
                <form action="/get_added_gene_details" method="get">
                    <div hidden><input value="${first_version}" type="text" name="first_version" size = "35" readonly="true"></div>
                    <div hidden><input value="${second_version}" type="text" name="second_version" size = "35" readonly="true"></div>
                    <button type="submit">Categorise All Added Genes</button>
                </form>
            </td>

            <td height="40">
                <form action="/get_removed_gene_details" method="get">
                    <div hidden><input value="${first_version}" type="text" name="first_version" size = "35" readonly="true"></div>
                    <div hidden><input value="${second_version}" type="text" name="second_version" size = "35" readonly="true"></div>
                    <button type="submit">Categorise All Removed Genes</button>
                </form>
            </td>
        </tr>

        <tr>
            <td height="40">
                <p>NO BUTTON</p>
            </td>

            <td height="40">
                <form action="/map_all_removed_genes" method="get">
                    <div hidden><input value="${first_version}" type="text" name="first_version" size = "35" readonly="true"></div>
                    <div hidden><input value="${second_version}" type="text" name="second_version" size = "35" readonly="true"></div>
                    <button type="submit">Show Transformation History of Removed Genes</button>
                </form>
            </td>
        </tr>

    </table>
</%block>