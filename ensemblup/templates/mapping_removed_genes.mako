<%inherit file="base.mako"/>
<%block name="partial_content">
<h2>${first_version} to ${second_version}</h2>
<h4>${mapping_summary_dict[0]}</h4>
<h4>${mapping_summary_dict[1]}</h4>
<h4>${mapping_summary_dict[2]}</h4>
<h4>${mapping_summary_dict[3]}</h4>
<br><br>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    {
                        text: 'TSV',
                        extend: 'csvHtml5',
                        fieldSeparator: '\t',
                        extension: '.tsv'
                    }
                ]
            } );
        } );
    </script>
    <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Old Stable Id</th>
                <th>Old Version</th>
                <th>New Stable Id</th>
                <th>New Version</th>
                <th>Mapping Session</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Old Stable Id</th>
                <th>Old Version</th>
                <th>New Stable Id</th>
                <th>New Version</th>
                <th>Mapping Session</th>
            </tr>
        </tfoot>
        <tbody>
            % for exp in examples:
            <tr>
                <% old_id = exp[0] %> <td>${old_id}</td>
                <% old_version = exp[1] %> <td>${old_version}</td>
                <% new_id = exp[2] %> <td>${new_id}</td>
                <% new_version = exp[3] %><td>${new_version}</td>
                <% mapping_session = exp[7] %><td>${mapping_session}</td>
            </tr>
            % endfor
        </tbody>>
    </table>
</%block>