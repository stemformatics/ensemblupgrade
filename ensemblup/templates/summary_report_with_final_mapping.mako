<%inherit file="base.mako"/>
<%block name="partial_content">
    <h2>Summary Report</h2>
    <h3>${first_version} to ${second_version}</h3><br>


    <br>

    <table width="70%"  border="1">
        <tr>
            <th height="40">&emsp;&emsp;Gene List Info</th>
            <th height="40">&emsp;&emsp;Number</th>
            <th height="40">&emsp;&emsp;Options</th>
        </tr>

       <tr>
           <td height="40">&emsp;&emsp;<b>Genes added to new version</b></td>
            <td height="40">&emsp;&emsp;${summary['added_id']}</td>
            <td height="40">
                <form action="/get_added_gene_details" method="get">
                    <div hidden><input value="${first_version}" type="text" name="first_version" size = "35" readonly="true"></div>
                    <div hidden><input value="${second_version}" type="text" name="second_version" size = "35" readonly="true"></div>
                    &emsp;&emsp;<button type="submit">Categorise All Added Genes</button>
                </form>
            </td>
        </tr>

        <tr>
            <td height="40">&emsp;&emsp;<b>Genes removed from old version</b></td>
            <td height="40">&emsp;&emsp;${summary['removed_id']}</td>
            <td height="40">
                <form action="/get_removed_gene_details" method="get">
                    <div hidden><input value="${first_version}" type="text" name="first_version" size = "35" readonly="true"></div>
                    <div hidden><input value="${second_version}" type="text" name="second_version" size = "35" readonly="true"></div>
                    &emsp;&emsp;<button type="submit">Categorise All Removed Genes</button>
                </form>
            </td>
        </tr>

        <tr>
            <td height="40">&emsp;&emsp;${first_version} Full List</td>
            <td height="40">&emsp;&emsp;${summary['first_version_num']}</td>
            <td height="40">
                <form action="/show_full_genes_list_details" method="get">
                    <div hidden><input value="${first_version}" type="text" name="version" size = "35" readonly="true"></div>
                    &emsp;&emsp;<button type="submit">See Full Genes Details</button>
                </form>
            </td>
        </tr>

        <tr>
            <td height="40">&emsp;&emsp;${second_version} Full List</td>
            <td height="40">&emsp;&emsp;${summary['second_version_num']}</td>
            <td height="40">
                <form action="/show_full_genes_list_details" method="get">
                    <div hidden><input value="${second_version}" type="text" name="version" size = "35" readonly="true"></div>
                    &emsp;&emsp;<button type="submit">See Full Genes Details</button>
                </form>
            </td>
        </tr>

        <tr>
            <td height="40">&emsp;&emsp;Genes with Same id in two versions</td>
            <td height="40">&emsp;&emsp;${summary['same_id_mapping']}</td>
            <td height="40">
                <form action="/get_removed_gene_details" method="get">
                    <div hidden><input value="${first_version}" type="text" name="first_version" size = "35" readonly="true"></div>
                    <div hidden><input value="${second_version}" type="text" name="second_version" size = "35" readonly="true"></div>
                    &emsp;&emsp;<button type="submit">See more details</button> 
                </form>
            </td>
        </tr>

        <tr>
            <td height="40">&emsp;&emsp;Genes mapped to other genes</td>
            <td height="40">&emsp;&emsp;${summary['multiple_id_mapping']}</td>
            <td height="40">
                <form action="/genes_mapped_to_other_genes" method="get">
                    <div hidden><input value="${first_version}" type="text" name="first_version" size = "35" readonly="true"></div>
                    <div hidden><input value="${second_version}" type="text" name="second_version" size = "35" readonly="true"></div>
                    &emsp;&emsp;<button type="submit">See more details</button>
                </form>
            </td>
        </tr>
        <tr>
            <td height="40">&emsp;&emsp;Genes rescued with Entrez</td>
            <td height="40">&emsp;&emsp;</td>
            <td height="40">
            </td>
        </tr>


        <tr>
            <td height="40">&emsp;&emsp;Gene Mapping History</td>
            <td height="40">&emsp;&emsp;----</td>
            <td height="40">
                <form action="/mapping_history" method="get">
                    <div hidden><input value="${first_version}" type="text" name="first_version" size = "35" readonly="true"></div>
                    <div hidden><input value="${second_version}" type="text" name="second_version" size = "35" readonly="true"></div>
                    &emsp;&emsp;<button type="submit">Check details</button>
                </form>
            </td>
        </tr>

        <tr>
            <td height="40">&emsp;&emsp;Removed Genes Mapping Draft</td>
            <td height="40">&emsp;&emsp;${summary['removed_id']}</td>
            <td height="40">
                <form action="/removed_genes_final_mapping" method="get">
                    <div hidden><input value="${first_version}" type="text" name="first_version" size = "35" readonly="true"></div>
                    <div hidden><input value="${second_version}" type="text" name="second_version" size = "35" readonly="true"></div>
                    &emsp;&emsp;<button type="submit">See more details</button>
                </form>
            </td>
        </tr>

        <tr>
            <td height="40">&emsp;&emsp;Full Mapping between two versions </td>
            <td height="40">&emsp;&emsp;${summary['first_version_num']}</td>
            <td height="40">
            </td>
        </tr>

    </table>
</%block>
