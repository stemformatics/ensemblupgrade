<%inherit file="base.mako"/>
<%block name="partial_content">
    <center>
	<h1>Mapping Links</h1>


        % for mapping in list_of_mappings:
            <% to_ensembl_version = mapping[1] %>
            <% from_ensembl_version = mapping[0] %>
            <a href="/generate_summary_report_with_final_mapping?first_version=${from_ensembl_version}&second_version=${to_ensembl_version}">From ${from_ensembl_version} to ${to_ensembl_version}</a>
            <br/>
            <br/>
        % endfor


    </center>
</%block>
