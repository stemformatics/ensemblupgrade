<%inherit file="base.mako"/>
<%block name="partial_content">
<h2>${first_version} to ${second_version}</h2>
<h2> -- Removed Gene Total List</h2>
<h2>${uncategorised_percent} genes not be categorised</h2>
<h3>Total number: ${total_num}</h3>
<h3>Uncategorised number: ${uncategorised}</h3>
<h3>Entrez Mapping number: ${entrez}</h3>
<h3>Haplotype number: ${haplotype}</h3>
<br><br>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    {
                        text: 'TSV',
                        extend: 'csvHtml5',
                        fieldSeparator: '\t',
                        extension: '.tsv'
                    }
                ]
            } );
        } );
    </script>
    <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Stable id</th>
                <th>Name</th>
                <th>Version</th>
                <th>Location</th>
                <th>Description</th>
                <th>Type</th>
                <th>Info</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Stable id</th>
                <th>Name</th>
                <th>Version</th>
                <th>Location</th>
                <th>Description</th>
                <th>Type</th>
                <th>Info</th>
            </tr>
        </tfoot>
        <tbody>
            % for exp in examples:
            <tr>
                <% stable_id = exp[0] %> <td>${stable_id}</td>
                <% name = exp[1] %><td>${name}</td>
                <% gene_version = exp[2] %><td>${gene_version}</td>
                <% location = exp[3] %><td>${location}</td>
                <% description = exp[4] %><td>${description}</td>
                <% type = exp[5] %><td>${type}</td>
                <% info = exp[7] %><td>${info}</td>

            </tr>
            % endfor
        </tbody>>
    </table>
</%block>