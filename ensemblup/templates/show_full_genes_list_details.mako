<%inherit file="base.mako"/>
<%block name="partial_content">
<h2>${version}</h2>
<br><br>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    {
                        text: 'TSV',
                        extend: 'csvHtml5',
                        fieldSeparator: '\t',
                        extension: '.tsv'
                    }
                ]
            } );
        } );
    </script>
    <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Stable id</th>
                <th>Name</th>
                <th>Version</th>
                <th>Location</th>
                <th>Description</th>
                <th>Type</th>
                <th>Entrez id</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Stable id</th>
                <th>Name</th>
                <th>Version</th>
                <th>Location</th>
                <th>Description</th>
                <th>Type</th>
                <th>Entrez id</th>
            </tr>
        </tfoot>
        <tbody>
            % for exp in examples:
            <tr>
                <% stable_id = exp[0] %> <td>${stable_id}</td>
                <% name = exp[1] %><td>${name}</td>
                <% gene_version = exp[2] %><td>${gene_version}</td>
                <% location = exp[3] %><td>${location}</td>
                <% description = exp[4] %><td>${description}</td>
                <% type = exp[5] %><td>${type}</td>
                <% entrez_id = exp[6] %><td>${entrez_id}</td>
            </tr>
            % endfor
        </tbody>>
    </table>
</%block>