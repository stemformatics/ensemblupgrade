<%inherit file="base.mako"/>
<%block name="partial_content">
<h2>${first_version} to ${second_version}</h2>
<br><br>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    {
                        text: 'TSV',
                        extend: 'csvHtml5',
                        fieldSeparator: '\t',
                        extension: '.tsv'
                    }
                ]
            } );
        } );
    </script>
    <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>${first_version} Stable Id</th>
                <th>${second_version} Stable Id</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>${first_version} Stable Id</th>
                <th>${second_version} Stable Id</th>
            </tr>
        </tfoot>
        <tbody>
            % for exp in examples:
            <tr>
                <% old_id = exp[0] %> <td>${old_id}</td>
                <% new_id = exp[1] %> <td>${new_id}</td>
            </tr>
            % endfor
        </tbody>>
    </table>


</%block>